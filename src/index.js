import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { Route, BrowserRouter as Router } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Items from './components/Items';
import ProductDetail from './components/List'
import Cart from './components/Cart';


ReactDOM.render(<Router>
    <div>
        <Route path='/' exact component={App}></Route>
        <Route path='/items' component={Items}></Route>
        <Route path="/product-detail" component={ProductDetail} />
        <Route path='/Mycart' component={Cart} />
    </div>
</Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
