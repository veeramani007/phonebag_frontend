import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './Items.css';
import axios from 'axios';

import { carouselProucts, midProducts, featureProducts } from './Products.json'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";



class Items extends Component {
    constructor(props) {
        super(props);

        this.state = {
            img: '',
            brand: '',
            name: '',
            price: '',
            qty: 1
        }

    }

    addToCart = (data) => {
        console.log("product:", data);

        var productDetails = {
            img: data.img,
            name: data.name,
            brand: data.brand,
            price: data.price,
            qty: 1
        }

        console.log("productDetails:", productDetails);

        axios.post('http://localhost:3000/cart/itemAdd', productDetails)
            .then(res => {
                console.log('response', res);

                toast.success("Item Successfully added to cart", {
                    autoClose: 2000
                });
                setTimeout(() => {
                }, 2100);

            }).catch((err) => {
                console.log(err, productDetails);
                toast.error("Something Went wrong!!", {
                    autoClose: 2000
                });
            });
    }

    storeDataLocal = (data) => {
        localStorage.setItem('refreshPrevent', JSON.stringify(data));
        console.log('local Item', localStorage.getItem('refreshPrevent'));
    }

    render() {

        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };

        return (
            <React.Fragment>
                <div>
                    <ToastContainer />
                    <div className="header col-md-12">
                        <a href="#default" className="logo">Phone baG</a>
                        <div className="header-right">
                            <a className="active" href="/items">Home</a>
                            <a href="#contact">Contact</a>
                            <a href="/Mycart">Cart</a>
                        </div>
                    </div>

                    <div className="container-fluid " style={{ borderBottom: "1px solid #ccc" }}>
                        <div className="col-md-12" >
                            <Slider {...settings}>
                                {carouselProucts.map((carouselProuct, index) =>
                                    <div className="col-md-12" key={index}>
                                        <div className='row'>
                                            <div className='col-md-6 my-2'>
                                                <img className='crouselImg' src={carouselProuct.img} alt=" " />
                                            </div>
                                            <div className='col-md-6 my-5 text-left'>
                                                <h1> {carouselProuct.brand + ' ' + carouselProuct.name} </h1>
                                                <p className='my-5' style={{ color: 'gray' }}>{carouselProuct.desc}
                                                </p>
                                                <Link to={{ pathname: '/product-detail', details: carouselProuct }}>
                                                    <button className='btn btn-dark my-5 rounded-0' onClick={() => this.storeDataLocal(carouselProuct)}>Shop now!</button>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </Slider>
                        </div>
                    </div>

                    {/* middle cards */}
                    <div className='container-fluid'>
                        <div className='col-md-12 my-2'>
                            <div className='row '>

                                {midProducts.map((midProduct, index) =>
                                    <div className='col-md-4 col-sm-12' key={index}>
                                        <div className='card shadow mb-4 bg-white rounded' >
                                            <div className="card-body">
                                                <div className='row'>
                                                    <div className='col-md-7 text-left'>
                                                        <h2>{midProduct.brand}</h2>
                                                        <h4 style={{ color: 'gray' }}>{midProduct.name}</h4>
                                                        <Link to={{ pathname: "/product-detail", details: midProduct }}>
                                                            <button className='btn btn-dark my-1' onClick={() => this.storeDataLocal(midProduct)}>Shop now!</button>
                                                        </Link>

                                                    </div>
                                                    <div className='col-md-5'>
                                                        <img className='ProductsImg' src={midProduct.img} alt='' />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>

                        {/* Featured */}
                        <div className='card cold-md-12 featured my-2 mb-3 text-left'>
                            <h4 className="d" style={{ marginLeft: '3rem' }}>Featured Products</h4>
                        </div>
                        {/* fea products */}
                        <div className=' my-1 col-md-12'>
                            <div className='row'>
                                {featureProducts.map((featureProduct, index) =>
                                    <div className='col-md-3 mb-3 hover-effect-demo' key={index}>
                                        <div className='card shadow-lg pb-3y'>
                                            <div className='card-body'>
                                                <img className='ProductsImg' src={featureProduct.img} alt="" />
                                                <h5 className='my-2 text-left' style={{ color: 'gray' }}>{featureProduct.brand + ' ' + featureProduct.name}</h5>
                                                <h5 className='my-1 mx-4 text-left'>${featureProduct.price}</h5>

                                                <button className='btn btn-dark mb-1 mx-3' onClick={() => this.addToCart(featureProduct)}>Add to cart</button>

                                                <Link to={{ pathname: "/product-detail", details: featureProduct }}>
                                                    <button className='btn btn-secondary mx-3' onClick={() => this.storeDataLocal(featureProduct)}>View</button>
                                                </Link>

                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                    {/* Footer */}
                    <footer className="apt-5 pb-4">
                        <div className="container">
                            <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet" />
                            <div className='my-1 col-md-12' id="contact">
                                <div className='row'>

                                    <div className="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                                        <h5 className="mb-4 font-weight-bold text-center">ABOUT US</h5>
                                        <ul className="f-address">
                                            <li>
                                                <div className="row">
                                                    <div className="col-1"><i className="fas fa-map-marker"></i></div>
                                                    <div className="col-10">
                                                        <h6 className="font-weight-bold mb-0">Address:</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="row">
                                                    <div className="col-1"><i className="far fa-envelope"></i></div>
                                                    <div className="col-10">
                                                        <h6 className="font-weight-bold mb-0">Have any questions?</h6>
                                                        <p><a href="#home">Support@userthemes.com</a></p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="row">
                                                    <div className="col-1"><i className="fas fa-phone-volume"></i></div>
                                                    <div className="col-10">
                                                        <h6 className="font-weight-bold mb-0">Address:</h6>
                                                        <p><a href="#data">+XX (0) XX XX-XXXX-XXXX</a></p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                                        <h5 className="mb-4 font-weight-bold text-center">FRESH TWEETS</h5>
                                        <ul className="f-address">
                                            <li>
                                                <div className="row">
                                                    <div className="col-1"><i className="fab fa-twitter"></i></div>
                                                    <div className="col-10">
                                                        <p className="mb-0"><a href="#about">@userthemesrel </a> HTML Version Out Now</p>
                                                        <label>10 Mins Ago</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="row">
                                                    <div className="col-1"><i className="fab fa-twitter"></i></div>
                                                    <div className="col-10">
                                                        <p className="mb-0"><a href="#about">@userthemesrel </a> HTML Version Out Now</p>
                                                        <label>10 Mins Ago</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="row">
                                                    <div className="col-1"><i className="fab fa-twitter"></i></div>
                                                    <div className="col-10">
                                                        <p className="mb-0"><a href="#about">@userthemesrel </a> HTML Version Out Now</p>
                                                        <label>10 Mins Ago</label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                                        <h5 className="mb-4 font-weight-bold text-center">SUPPORT</h5>
                                        <ul className="f-address">
                                            <li>
                                                <div className="row">
                                                    <div className="col-1"><i className="far fa-life-ring"></i></div>
                                                    <div className="col-10">
                                                        <p className="mb-0"><a href="#about">Customer </a>Affiliate</p>
                                                        <label>Update</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="row">
                                                    <div className="col-1"><i className="far fa-life-ring"></i></div>
                                                    <div className="col-10">
                                                        <p className="mb-0"><a href="#about">Data </a> LogIn</p>
                                                        <label>Open resource</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="row">
                                                    <div className="col-1"><i className="far fa-life-ring"></i></div>
                                                    <div className="col-10">
                                                        <p className="mb-0"><a href="#about" style={{ color: 'white' }}>Value </a> Product</p>
                                                        <label>Total Entry</label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
                                        <h5 className="mb-4 font-weight-bold">CONNECT WITH US</h5>
                                        <div className="input-group">
                                            <input type="text" className="form-control" placeholder="Your Email Address"></input>
                                            <span className="input-group-addon" id="basic-addon2"><i className="fas fa-check"></i></span>

                                            <ul className="social-pet mt-4">
                                                <li><a href="#about" title="facebook"><i className="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#about" title="twitter"><i className="fab fa-twitter"></i></a></li>
                                                <li><a href="#about" title="google-plus"><i className="fab fa-google-plus-g"></i></a></li>
                                                <li><a href="#about" title="instagram"><i className="fab fa-instagram"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>

                    <section className="copyright">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12 ">
                                    <div className="text-center text-white">
                                        &copy; 2018 Your Company. All Rights Reserved.
						</div>
                                </div>
                            </div>
                        </div>
                    </section>


                </div>
            </React.Fragment >
        )
    }
}

export default Items;