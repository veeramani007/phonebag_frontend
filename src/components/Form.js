import React, { Component } from 'react';

import * as ReactBootstrap from 'react-bootstrap';
import { MDBDataTable } from 'mdbreact';
import './Form.css';
import axios from 'axios';

class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
            itemName: '',
            cat: '',
            qty: '',
            price: '',
            list: ''
        };

    }


    componentDidMount() {
        fetch('http://localhost:3000/items/allItems')
            .then(response => response.json())
            .then(result => {
                console.log("result", result)
                this.setState({ list: result })
            })
        // this.alData(result);

    }

    change = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSub = (e) => {
        e.preventDefault()
        console.log("data", this.state)
        const details = {

            name: this.state.itemName,
            category: this.state.cat,
            qty: this.state.qty,
            price: this.state.price
        };

        axios.post('http://localhost:3000/items/addItem', details)
            .then(response => {
                console.log("pass", response);
                this.setState({
                    itemName: '',
                    cat: '',
                    qty: '',
                    price: ''
                })
            })
            .catch(function (error) {
                console.log("fail", error);
            });
    }

    render() {
        console.log("list", this.state.list);

        const columns = [{
            label: 'ID',
            field: 'id',
            sort: 'asc',
            width: 150
        },

        {
            label: 'Name',
            field: 'name',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Category',
            field: 'category',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Quantity',
            field: 'qty',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Price',
            field: 'price',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Options',
            field: '',
            sort: 'asc',
            width: 150
        }]


        return (
            <React.Fragment >
                <div className='container'>
                    <div className='p'>
                        <h1>Item Creation page</h1>
                    </div>

                    <div className="col-md-12">
                        <div className="row">
                            <ReactBootstrap.Card className="card shadow p-3 mb-5 bg-white rounded col-md-4" border='priamry-round'>
                                <h4>Add the new item </h4>
                                <form onSubmit={(e) => this.handleSub(e)} className='formCss'>

                                    <label>Item Name :</label>

                                    <ReactBootstrap.InputGroup className="mb-4 col-lg-12">
                                        <ReactBootstrap.FormControl
                                            placeholder="item name"
                                            name='itemName'
                                            type='text'
                                            value={this.state.itemName}
                                            onChange={(e) => this.change(e)}
                                            autoComplete='off'
                                            required
                                        />
                                    </ReactBootstrap.InputGroup>


                                    <label>Item Category :</label>

                                    <ReactBootstrap.Dropdown
                                    >
                                        <select
                                            className='dropdown'
                                            value={this.state.cat}
                                            onChange={e => this.change(e)}
                                            name="cat"
                                            required
                                        >
                                            <option selected value="">Type</option>
                                            <option value="fruits">Fruits</option>
                                            <option value="vegitables">Vegitables</option>
                                            <option value="BeautyandHygiene ">Beauty & Hygiene </option>
                                            <option value="personalCare">Personal Care</option>
                                            <option value="dairy ">Dairy </option>
                                        </select>

                                    </ReactBootstrap.Dropdown>


                                    <label>Item Quantity :</label>
                                    <ReactBootstrap.FormControl
                                        placeholder="Quantity"
                                        name='qty'
                                        type='number'
                                        value={this.state.qty}
                                        onChange={e => this.change(e)}
                                        required
                                    />

                                    <label>Item price :</label>
                                    <ReactBootstrap.FormControl
                                        placeholder="Price"
                                        name='price'
                                        type='number'
                                        value={this.state.price}
                                        onChange={e => this.change(e)}
                                        required
                                    />
                                    <br></br>
                                    <button type="submit" class="btn btn-primary">Submit</button>

                                </form>
                            </ReactBootstrap.Card>
                        </div>
                        <MDBDataTable
                            striped
                            bordered
                            hover
                            columns={columns}
                            rows={this.state.list}
                        />
                    </div>


                </div>
            </React.Fragment >

        );
    }
}

export default Form;