import React, { Component } from 'react';
import './AddUser.css';


import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';

class AddUser extends Component {
    constructor(props) {
        super(props);

        this.state = {

            firstname: '',
            lastname: '',
            email: '',
            phone: '',
            address: '',
            cat: '',
            count: '',
            date: '',
            errormsg: ["", "", ""]
        }
    }

    change = (e) => {

        // this.setState({
        //     [e.target.name]: e.target.value
        // });


        if (e.target.name === 'firstname') {
            if (e.target.value === '' || e.target.value === null) {
                console.log('Err name:', e.target.name, 'value:', e.target.value);
                this.setState({
                    firstnameError: true
                })
            } else {
                console.log('Succ name:', e.target.name, 'value:', e.target.value);
                this.setState({
                    firstnameError: false,
                    firstName: e.target.value
                })
            }
        }

        // switch (fieldname) {
        //     case "firstname":
        //         if (this.state.firstname === "") {
        //             this.setState({ errormsg: "First name is required" })
        //         }
        //         else {
        //             this.setState({
        //                 firstname: e.target.value
        //             });
        //         }

        //         break;
        //     default:
        // }
    };

    formSub = (e) => {
        e.preventDefault();
        const formData = {

            firstname: this.state.firstname,
            lastname: this.state.lastname,
            email: this.state.email,
            phone: this.state.phone,
            address: this.state.address,
            category: this.state.cat,
            count: this.state.count,
            date: this.state.date

        };

        axios.post('http://localhost:3000/users/newUser', formData)
            .then(res => {
                console.log("Form Data", res);
                this.notify();
                this.cancel();

                setTimeout(() => {
                    this.props.history.push('/userData')
                }, 1100);

            }).catch((err) => {
                console.log('err', err, 'data', formData);

            });
    };

    //Toast bar

    notify = () => {
        toast.success("User added successfully!!", {
            autoClose: 1000
        });
    }

    cancel = () => {
        this.setState({
            firstname: '',
            lastname: '',
            email: '',
            phone: '',
            address: '',
            cat: '',
            count: '',
            date: ''
        });
        console.log('Form data cleared');

    }

    render() {
        return (
            <React.Fragment>
                <div className='container'>
                    <h4 className='heading'> Add User details... </h4>
                    <ToastContainer />

                    <div className="card Bcard">
                        <div className='card-body'>
                            <form onSubmit={(e) => this.formSub(e)} className='form'>
                                {/* <p className='note'>*All fields are Mandatory</p> */}
                                <div className='col-md-12'>
                                    {/* <div className='card-header mb-4 Hcard col-md-12'></div> */}
                                    <div className='row'>

                                        <div className="col-md-6 my-3">
                                            <div class="floating-label">
                                                <input class="floating-input"
                                                    type="text"
                                                    placeholder=" "
                                                    name='firstname'
                                                    value={this.state.firstname}
                                                    onChange={(e) => this.change(e)}
                                                    autoComplete='off'
                                                ></input>

                                                {/* <span style={{ color: "red" }}>{this.state.errormsg}</span> */}
                                                {this.state.firstnameError ? <span style={{ color: "red" }}>Please Enter some value</span> : ''}
                                                <span class="highlight"></span>
                                                <label>First Name</label>
                                            </div>
                                        </div>

                                        <div className="col-md-6 my-3">
                                            <div class="floating-label">
                                                <input class="floating-input"
                                                    type="text"
                                                    placeholder=" "
                                                    name='lastname'
                                                    value={this.state.lastname}
                                                    onChange={(e) => this.change(e)}
                                                    autoComplete='off'></input>

                                                <span class="highlight"></span>
                                                <label>Last Name</label>
                                            </div>
                                        </div>

                                        <div className="col-md-6 my-3">
                                            <div class="floating-label">
                                                <input class="floating-input"
                                                    type="email"
                                                    placeholder=" "
                                                    name='email'
                                                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                                                    value={this.state.email}
                                                    onChange={(e) => this.change(e)}
                                                    autoComplete='off'></input>

                                                <span class="highlight"></span>
                                                <label>Email</label>
                                            </div>
                                        </div>

                                        <div className="col-md-6 my-3">
                                            <div class="floating-label">
                                                <input class="floating-input"
                                                    type="number"
                                                    placeholder=" "
                                                    maxlength="10"
                                                    name='phone'
                                                    value={this.state.phone}
                                                    onChange={(e) => this.change(e)}
                                                    autoComplete='off'></input>

                                                <span class="highlight"></span>
                                                <label>Phone no</label>
                                            </div>
                                        </div>

                                        <div className="col-md-6 my-3">
                                            <div class="floating-label">
                                                <input class="floating-input"
                                                    type="text"
                                                    placeholder=" "
                                                    name='address'
                                                    value={this.state.address}
                                                    onChange={(e) => this.change(e)}
                                                    autoComplete='off'></input>

                                                <span class="highlight"></span>
                                                <label>Address</label>
                                            </div>
                                        </div>

                                        <div className="col-md-6 my-3">
                                            <div className="floating-label">
                                                <select className="floating-select"
                                                    value={this.state.cat}
                                                    onChange={e => this.change(e)}
                                                    name="cat">
                                                    <option value="">Select</option>
                                                    <option value="fruits">Fruits</option>
                                                    <option value="vegitables">Vegitables</option>
                                                    <option value="BeautyandHygiene ">Beauty & Hygiene </option>
                                                    <option value="personalCare">Personal Care</option>
                                                    <option value="dairy ">Dairy </option>
                                                </select>
                                                {/* <span class="highlight"></span>
                                                <label>Select</label> */}
                                            </div>
                                        </div>

                                        <div className="col-md-6 my-3">
                                            <div class="floating-label">
                                                <input class="floating-input"
                                                    type="number"
                                                    placeholder=" "
                                                    name='count'
                                                    value={this.state.count}
                                                    onChange={(e) => this.change(e)}
                                                    autoComplete='off'></input>

                                                <span class="highlight"></span>
                                                <label>Count</label>
                                            </div>
                                        </div>

                                        <div className="col-md-6 my-3">
                                            <div class="floating-label">
                                                <input class="floating-input"
                                                    type="date"
                                                    placeholder=" "
                                                    name='date'
                                                    value={this.state.date}
                                                    onChange={(e) => this.change(e)}
                                                    autoComplete='off'></input>

                                                <span class="highlight"></span>
                                                <label>Date</label>
                                            </div>
                                        </div>


                                        <div className="col-md-12 text-center">
                                            <button type='submit' className='btn btn-primary mx-2'>Submit</button>
                                            <button onClick={this.cancel} className='btn btn-danger mx-2'>Cancel</button>

                                        </div>

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </React.Fragment>
        );
    }
}

export default AddUser;