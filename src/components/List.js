import React, { Component } from 'react';
import './List.css';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';

class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            img: '',
            brand: '',
            name: '',
            desc: '',
            price: '',
            qty: 1,
            qtyError: ''

        }

    }

    componentDidMount() {

        console.log("detail", this.props.location.details)


        var data = JSON.parse(localStorage.getItem('refreshPrevent'));
        // var data = this.props.location.details;
        console.log('data', data);

        this.setState({
            img: data.img,
            brand: data.brand,
            name: data.name,
            desc: data.desc,
            price: data.price

        })
    }
    change = (e) => {
        if (e.target.name === 'qty') {
            if (e.target.value === '' || e.target.value === null) {
                console.log("empty", e.target.value);
                this.setState({
                    qtyError: true,
                    [e.target.name]: e.target.value
                })
            } else {
                this.setState({
                    qtyError: false,
                    [e.target.name]: e.target.value
                })
                console.log("state", e.target.name, e.target.value);
            }

        }

    }

    addToCart = () => {
        const productDetails = {
            img: this.state.img,
            brand: this.state.brand,
            name: this.state.name,
            price: this.state.price,
            qty: this.state.qty

        };

        axios.post('http://localhost:3000/cart/itemAdd', productDetails)
            .then(res => {
                console.log('response', res);

                toast.success("Item Successfully added to cart", {
                    autoClose: 1000
                });
                setTimeout(() => {
                    this.props.history.push('/Mycart')
                }, 1100);
            }).catch((err) => {
                console.log(err, productDetails);
                toast.error("Something Went wrong!!", {
                    autoClose: 1000
                });
            });
    }

    render() {

        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };

        return (
            <React.Fragment>

                <div class="header col-md-12">
                    <a href="#default" className="logo">Phone baG</a>
                    <div class="header-right">
                        <a href="/items">Home</a>
                        <a class="" href="#contact" >Contact</a>
                        <a href="/Mycart">Cart</a>
                    </div>
                </div>

                <div className='container'>
                    <ToastContainer />
                    <div className='col-md-12 my-3'>
                        <div className='row'>

                            <div className='col-md-6'>
                                <div className='card shadow-sm'>
                                    <Slider {...settings}>
                                        <div>
                                            <img src={this.state.img} alt=" " />
                                        </div>
                                        <div>
                                            <img src={this.state.img} alt=" " />
                                        </div>
                                        <div>
                                            <img src={this.state.img} alt=" " />
                                        </div>
                                        <div>
                                            <img src={this.state.img} alt=" " />
                                        </div>
                                        <div>
                                            <img src={this.state.img} alt=" " />
                                        </div>
                                        <div>
                                            <img src={this.state.img} alt=" " />
                                        </div>
                                    </Slider>
                                </div>
                            </div>

                            <div className='col-md-6 my-5 text-left'>
                                <h1 className='mb-5'>{this.state.brand + ' ' + this.state.name}</h1>
                                <h2 className='mb-2'>${this.state.price}</h2>
                                <h5 className="mb-3" style={{ color: 'gray' }}>{this.state.desc}</h5>
                                <div className='count mt-2 row'>
                                    <div className="card rounded-0 border-right-0 border-left-0">
                                        <div className="card-body">
                                            <input type="number" className="mx-5" name="qty" value={this.state.qty} onChange={(e) => this.change(e)} min="1" max="8" ></input>
                                            <button className="btn btn-dark rounded-0" disabled={!this.state.qty} onClick={this.addToCart}>ADD TO CART</button>
                                            <br />
                                            {this.state.qtyError ? <span style={{ color: "red" }}>Please Enter valid Quantity</span> : ''}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default List;