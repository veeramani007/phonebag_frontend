import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Breadcrumb from '../../common/breadcrumb.component'
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import SweetAlert from 'react-bootstrap-sweetalert';

// css files
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';

//json file
var data = require('../../../assets/json/downloads');

class Downloads extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: data,
            alert: null,
            show: false,
            basicTitle: '',
            basicType: "default",
        };
    }

    closeAlert = () => {
        this.setState({
            show: false
        });
    }

    buttonExample = (type) => {
        this.setState({
            alert: (
                <SweetAlert
                    showCancel
                    confirmBtnText="Continue"
                    confirmBtnBsStyle={type}
                    type={type}
                    title="Are you sure?"
                    onCancel={this.hideAlert}
                    onConfirm={this.hideAlert}
                >
                    You will not be able to recover this data!
                    </SweetAlert>
            )
        });
    }

    hideAlert = () => {
        this.setState({
            alert: null
        });
    }
    
    render() {
        var data = this.state.data;
        const { SearchBar } = Search;
        const linkvalue = () => {
            return(

            <div className="onhover-dropdown action-table">
               <div className="media  align-items-center">
                   <div className="media-body">

                       <a className="txt-dark"><button className="btn btn-primary">
                           <i class="fa fa-cog"></i></button></a>
                   </div>
               </div>
               <ul className="profile-dropdown onhover-show-div">
                   <li className="border-colour1">
                   <Link to={`/applications/users/edit-user`}>
                          View / Edit
                       </Link>
                   </li>
                   <li className="border-colour2" onClick={() => this.buttonExample()}>
                           Delete
                   </li>
                   <li className="border-colour3">
                           Hide
                  </li>
               </ul>
           </div>
         )
        }

        const LinkforView =(cell, row) =>{
            return(
                <Link to ={`/reports/Addproductsearchterm`}>Test-Premium Plan</Link>
            )
        }

        const columns = [{
            dataField: 'Product',
            sort: true,
            align: 'center',
            filter: textFilter({placeholder: 'Product'}),
        },{
            dataField: 'Link',
            sort: true,
            align: 'center',
            formatter:LinkforView,
            filter: textFilter({placeholder: 'Link'})
        } ,
        {
            dataField: 'SKU',
            sort: true,
            align: 'center',
            filter: textFilter({placeholder: 'SKU'})

        } ,
        {
            dataField: 'Purchases',
            sort: true,
            align: 'center',
            filter: textFilter({placeholder: 'Purchases'}) 
        },
        {
            dataField: 'Downloads',
            sort: true,
            align: 'center',
            filter: textFilter({placeholder: 'Downloads'}) 
        }
        
       
        ];

        const defaultSorted = [{
            dataField: 'id',
            order: 'asc'
        }];

        const pageButtonRenderer = ({
                                        page,
                                        active,
                                        disable,
                                        title,
                                        onPageChange
                                    }) => {
            const handleClick = (e) => {
                e.preventDefault();
                onPageChange(page);
            };
            var classname = '';
                classname = 'btn btn-primary';
            return (
                <li className="page-item pl-1" key={page}>
                    <a href="#" onClick={ handleClick } className={ classname } >{ page }</a>    
                </li>
            );
        };

        const options = {
            pageButtonRenderer
        };

      
        return (
            <div className="orders-list">

                <Breadcrumb title="Downloads Report" parent="Reports" />
                <div className="container-fluid">
                    {this.state.alert}
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-body">
                            <div className="datatable-react">
                                   <ToolkitProvider
                                        keyField="id"
                                        data={data}
                                        columns={columns}
                                        search

                                    >
                                        {
                                            props => (
                                                <div >
                                                    <div class="col-md-12 p-b-10">
                                                        <div className="row">
                                                            <div className="col-md-3 p-l-0 float-left search-bar">
                                                                <SearchBar {...props.searchProps} className="m-b-5" />
                                                            </div>
                                                            <div className="col-md-3 records-count"><h6>2 Records Found</h6></div>
                                                            </div>
                                                    </div>
                                                
                                                <div className="alter-table">
                                                    <BootstrapTable
                                                        {...props.baseProps}
                                                        defaultSorted={defaultSorted}
                                                        filter={filterFactory()}
                                                        pagination={paginationFactory(options)}  />
                                                </div>
                                                </div>
                                            )
                                        }
                                    </ToolkitProvider>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <br />
            </div>
        )
    }
}

export default Downloads;