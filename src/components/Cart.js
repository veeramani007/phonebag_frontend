import React, { Component } from 'react';
import './Cart.css';
import Axios from 'axios';

import swal from 'sweetalert';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Cart extends Component {

    constructor(props) {
        super(props);
        this.state = {            
            datas: [],
            SnackbarShow: false,
            snackMsg: ''
        }

    }

    // REFRESH THE COMPONENT
    componentDidMount() {

        fetch('http://localhost:3000/cart/showcart')
            .then(response => response.json())
            .then(Items => {
                this.setState({ datas: Items });

                // const Totalamount = this.state.datas.reduce((price, data) => price + (data.price * data.qty), 0);

                // console.log("Totalamount", Totalamount); 
            })
    }

        // CLOSE SNACKBAR
        snackClose = () => {
            this.setState({ SnackbarShow: false })
        }
    
        // ORDER PLACED, CLEAR CART AND REDIRECT TO HOME PAGE
        placeorder = () => {
            Axios.delete('http://localhost:3000/cart/dltAll')
            .then(res => {
                console.log('success', res);
    
                toast.success("Order placed SuccessFully", {
                    autoClose: 1000
                });
                this.componentDidMount();
    
            }).catch((err) => {
                console.log('err', err);
            });
    
            setTimeout(() => {
                this.props.history.push('/items')
            }, 1100);
        }
    
        //CLEAR CART
        clearcart=() =>{
    
            swal({
                title: "Are you sure?",
                text: "Remove all items from cart",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((dlt) => {
                if (dlt) {
                    Axios.delete('http://localhost:3000/cart/dltAll')
                        .then(res => {
                            console.log('success', res);
    
                            swal("Cart is Empty now", {
                                buttons: false,
                                timer: 1500,
                                icon: "success",
                            });
                            this.componentDidMount();
                        }).catch((err) => {
                            console.log('err', err);
                            swal("Sorry something went wrong", {
                                buttons: false,
                                timer: 1500,
                                icon: "error",
                            });
                        });
                }
            });
        }
    //DELETE ITEMS FROM CART
    deleteItem = (id) => {

        swal({
            title: "Are you sure?",
            text: "need to remove from cart",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((dlt) => {
            if (dlt) {
                Axios.delete('http://localhost:3000/cart/remove_item/' + id)
                    .then(res => {
                        console.log('success', res);

                        swal("Item removed from cart", {
                            buttons: false,
                            timer: 1500,
                            icon: "success",
                        });
                        this.componentDidMount();
                    }).catch((err) => {
                        console.log('err', err);
                        swal("Sorry something went wrong", {
                            buttons: false,
                            timer: 1500,
                            icon: "error",
                        });
                    });
            }
        });
    }

    // INCREASE DECREASE QUANTITY COUNTER
    incDec = (value, index) => {

        console.log("Qty", this.state.datas[index].qty, "index", index);

        var c=this.state.datas[index].qty;

        if (value === 1) {
        c+=1;
    } else if(value===-1) {
        c-=1;
        console.log("decrement");
    }
        if(c>0 && c<10){

        Axios.put('http://localhost:3000/cart/edit_Item/' + this.state.datas[index]._id, { qty: c })
        .then(res => {
                console.log('updateSuccess', res);
            this.setState({ SnackbarShow: true, snackMsg: 'item "' + this.state.datas[index].brand + " " + this.state.datas[index].name + '" quantity changed to ' + res.data.$set.qty });
            
            this.componentDidMount();

            })
                .catch((err) => {
                this.setState({ SnackbarShow: true, snackMsg: "Quantity not changed" });
                console.log('updateErr', err);
             });
        }
        console.log("changed to", c);

    }

    // CHANGE QUANTITYBY KEYPRESS EVENT
    changeQty = (e, brand, name, id) => {

        if (e.target.value > 0 &&  e.target.value !== " ") {

            this.setState({
                [e.target.name]: e.target.value
            });

            Axios.put('http://localhost:3000/cart/edit_Item/' + id, { [e.target.name]: e.target.value })
                .then(res => {
                    console.log('updateSuccess', res);
                    this.setState({ SnackbarShow: true, snackMsg: 'item "' + brand + " " + name + '" quantity changed to '  + res.data.$set.qty });
                    this.componentDidMount();
                }).catch((err) => {
                    this.setState({ SnackbarShow: true, snackMsg: "Quantity not changed" });
                    console.log('updateErr', err);
                });

        } else {
            e.target.value = null
        }
    }

    // HTML RENDER
    render() {
        console.log('cartData', this.state.datas);

        const isEmpty = !this.state.datas.length;
        return (
            <React.Fragment>
                <div>
                    <ToastContainer />
                    <div className="header col-md-12">
                        <a href="#default" className="logo">Phone baG</a>
                        <div className="header-right">
                            <a href="/items">Home</a>
                            <a href="/items#contact" >Contact</a>
                            <a className="active" href="#cart">Cart</a>
                        </div>
                    </div>

                    <div className="container my-4">
                        <div className="col-md-12">
                            <div className="row">

                                <Snackbar anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'center',
                                }}
                                    open={this.state.SnackbarShow}
                                    autoHideDuration={2000}
                                    onClose={this.snackClose}
                                    message={<span id="message-id">{this.state.snackMsg}</span>}

                                    action={[
                                        <IconButton
                                            key="close"
                                            aria-label="close"
                                            color="inherit"
                                            onClick={this.snackClose}
                                        ><img alt=" " src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCA
                                        yMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0S
                                        C0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEw
                                        LTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwek04IDE1bC01LTUgMS40MS0xLjQxTDggMTIuMTdsNy41OS03LjU5TDE3IDZsL
                                        TkgOXonIGZpbGw9JyMyNkJDNEUnLz4KCTwvZz4KPC9zdmc+Cg==" /> </IconButton>,
                                    ]} />

                                <div className="col-md-6 ">
                                    <div className="card mb-1 shadow rounded-0">
                                        <div className="card-header text-left cardHead">My Cart({this.state.datas.length})</div>
                                        <div className="card-body  pb-0">
                                          
                                          <div>{( isEmpty
                                            ? <div className="text-center">
                                                <img style={{height:'11rem'}} src="https://rukminim1.flixcart.com/www/800/800/promos/16/05/2019/d438a32e-765a-4d8b-b4a6-520b560971e8.png?q=90" alt=""/><p >Sorry, the cart is empty now.</p></div>
                                            : <div>                                      
                                            {this.state.datas.map((data, index) =>
                                                <div className="row itemImg" key={index}>
                                                    <div className='col-md-4 my-2 '>
                                                        <img src={data.img} alt="" />
                                                        <button type="button" className="btn btn-info " onClick={() => this.incDec(-1,index)}>-</button> &nbsp;
                                                        <input className="my-2  text-center" type="number" name="qty" min="1" max="9" value={data.qty} ref={(input) => this.input = (input)} onChange={(e) => this.changeQty(e, data.brand, data.name, data._id)}></input>
                                                        &nbsp;
                                                        <button type="button" className="btn btn-info " onClick={() => this.incDec(1, index)}>+</button>
                                                    </div>

                                                    <div className="col-md-4 text-left my-2">
                                                        <h4 className="my-2  mx-2">{data.brand}</h4>
                                                        <h5 className="my-2  mx-2">{data.name}</h5>
                                                        <div className="my-4 mx-2 row">
                                                            <h5 >${data.price}</h5>
                                                            &nbsp;&nbsp;
                                                            <small className="my-1">per item</small>
                                                        </div>

                                                        <div className="row my-3 mx-2">
                                                            <h6>QTY.TOTAL:</h6>
                                                            <h6 className="font-weight-bold" style={{ color: '#388E3C' }}>{data.qty * data.price} $</h6>

                                                        </div>
                                                    </div>
                                                    <div className="col-md-4 mt-5">
                                                        <div className="col my-4 text-center">
                                                            <button type="button" className="btn btn-danger rounded-0 remove" onClick={() => this.deleteItem(data._id)}>REMOVE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            )}
                                            </div>
                                            )}
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                                {/* PRICE DETAILS */}
                                <div className="col-md-5 priceData">
                                    <div className="card shadow-lg rounded-0">
                                        <div className="card-header  " style={{ color: '#929292' }}>PRICE DETAILS</div>
                                        <div className="card-body">

                                            <div className="row my-2">
                                                <div className="col text-left">
                                                    <h6>Price({this.state.datas.length} items)</h6>
                                                </div>
                                                <div className="col text-right">
                                                    <h6>$ {this.state.datas.reduce((price, data) => price + (data.price * data.qty), 0)}</h6>
                                                </div>
                                            </div>

                                            <div className="row my-3 itemImg" >
                                                <div className="col text-left">
                                                    <h6>Delivery</h6>
                                                </div>
                                                <div className="col text-right">
                                                    <h6 style={{ color: '#388E3C' }}>FREE</h6>
                                                </div>
                                            </div>

                                            <div className="row mt-4">
                                                <div className="col text-left">
                                                    <h6>Total Payable</h6>
                                                </div>
                                                <div className="col text-right">
                                                    <h6 >$ {this.state.datas.reduce((price, data) => price + (data.price * data.qty), 0)}</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-footer">
                                          <div className="row">
                                             <div className="col text-left">
                                                <button type="button" className="btn btn-danger rounded-0" disabled={isEmpty} onClick={this.clearcart}>CLEAR CART</button>
                                            </div>

                                            <div className="col text-right">
                                                <button type="button" className="btn btn-primary rounded-0" disabled={isEmpty} onClick={this.placeorder}>PLACE ORDER</button>
                                            </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    {/* SNACK BAR */}

                </div>
            </React.Fragment >
        )
    }
}

export default Cart;